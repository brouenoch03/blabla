import 'package:blabla/src/models/taxi.dart';
import 'package:blabla/src/models/taxi_type.dart';

class TaxiController {
  static Future<List<Taxi>> getTaxis() async {
    return [
      const Taxi.named(
          plateNo: "",
          isAvailable: true,
          taxiType: TaxiType.Standard,
          id: "1",
          title: "Standard"),
      const Taxi.named(
          plateNo: "",
          isAvailable: true,
          taxiType: TaxiType.Premium,
          id: "2",
          title: "Premium"),
      const Taxi.named(
          plateNo: "",
          isAvailable: true,
          taxiType: TaxiType.Platinum,
          id: "3",
          title: "Standard"),
    ];
  }
}
