import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:blabla/src/bloc/taxi_booking_bloc.dart';
import 'package:blabla/src/bloc/taxi_booking_event.dart';

class TaxiBookingCancellationDialog extends StatelessWidget {
  const TaxiBookingCancellationDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Cancel Ride"),
      content: const Text("Do you want to cancel ride?"),
      actions: <Widget>[
        TextButton(
          child: const Text(
            "Cancel",
            style: TextStyle(fontSize: 16.0),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: const Text(
            "Ok",
            style: TextStyle(fontSize: 16.0),
          ),
          onPressed: () {
            BlocProvider.of<TaxiBookingBloc>(context)
                .add(TaxiBookingCancelEvent());
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}
