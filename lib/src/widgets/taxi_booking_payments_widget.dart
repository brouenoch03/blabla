import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:blabla/src/bloc/taxi_booking_bloc.dart';
import 'package:blabla/src/bloc/taxi_booking_event.dart';
import 'package:blabla/src/bloc/taxi_booking_state.dart';
import 'package:blabla/src/models/payment_method.dart';
import 'package:blabla/src/widgets/rounded_button.dart';

class TaxiBookingPaymentsWidget extends StatefulWidget {
  @override
  _TaxiBookingPaymentsWidgetState createState() =>
      _TaxiBookingPaymentsWidgetState();
}

class _TaxiBookingPaymentsWidgetState extends State<TaxiBookingPaymentsWidget> {
  List<PaymentMethod> methods = [];
  late PaymentMethod selectedMethod;

  @override
  void initState() {
    super.initState();
    methods = (BlocProvider.of<TaxiBookingBloc>(context).state
            as PaymentNotInitializedState)
        .methodsAvaiable;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      "Select Payment",
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                    ListView.separated(
                      itemBuilder: (context, index) {
                        return buildPaymentMethod(methods[index]);
                      },
                      padding: const EdgeInsets.symmetric(vertical: 24.0),
                      separatorBuilder: (context, index) => Container(
                        height: 18.0,
                      ),
                      physics: const ClampingScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: methods.length,
                    ),
                    Text(
                      "Promo Code",
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                    const SizedBox(
                      height: 18.0,
                    ),
                    buildInputWidget('', "Add promo code", () {})
                  ],
                ),
              ),
            ),
          ),
          Row(
            children: <Widget>[
              RoundedButton(
                onTap: () {
                  BlocProvider.of<TaxiBookingBloc>(context)
                      .add(BackPressedEvent());
                },
                iconData: Icons.keyboard_backspace,
              ),
              const SizedBox(
                width: 18.0,
              ),
              Expanded(
                flex: 2,
                child: RoundedButton(
                  text: "Confirm Payment",
                  onTap: () {
                    BlocProvider.of<TaxiBookingBloc>(context)
                        .add(PaymentMadeEvent(paymentMethod: selectedMethod));
                  },
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget buildPaymentMethod(PaymentMethod method) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedMethod = method;
        });
      },
      child: Container(
        padding: const EdgeInsets.all(12.0),
        decoration: BoxDecoration(
            color: const Color(0xffeeeeee).withOpacity(0.5),
            borderRadius: BorderRadius.circular(12.0)),
        child: Row(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(16.0),
              child: Image.network(
                method.icon,
                width: 56.0,
                height: 56.0,
                fit: BoxFit.cover,
              ),
            ),
            const SizedBox(
              width: 16.0,
            ),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text(
                    method.title,
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                  const SizedBox(
                    height: 4.0,
                  ),
                  Text(
                    method.description,
                    style: Theme.of(context).textTheme.titleSmall,
                  ),
                ],
              ),
            ),
            selectedMethod == method
                ? const Icon(
                    Icons.check_circle,
                    size: 28.0,
                  )
                : Container(
                    width: 0,
                    height: 0,
                  )
          ],
        ),
      ),
    );
  }

  Widget buildInputWidget(String? text, String? hint, Function() onTap) {
    if (text == null && hint == null) {
      throw ErrorDescription('text or hint required');
    }

    return Container(
      padding: const EdgeInsets.symmetric(vertical: 24.0, horizontal: 16.0),
      decoration: BoxDecoration(
        color: const Color(0xffeeeeee).withOpacity(0.5),
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: Text(
        text ?? hint ?? '',
        style: Theme.of(context)
            .textTheme
            .titleMedium!
            .copyWith(color: text == null ? Colors.black45 : Colors.black),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }
}
