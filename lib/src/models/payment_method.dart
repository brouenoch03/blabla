import 'package:equatable/equatable.dart';

class PaymentMethod extends Equatable {
  final String id;
  final String title;
  final String icon;
  final String description;

  const PaymentMethod({
    required this.id,
    required this.title,
    required this.icon,
    required this.description
  });

  @override
  List<Object> get props => [id, title, icon, description];
}
