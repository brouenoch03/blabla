import 'package:equatable/equatable.dart';

class TaxiDriver extends Equatable {
  final String id;
  final String driverName;
  final double driverRating;
  final String taxiDetails;
  final String driverPic;

  const TaxiDriver(this.id, this.driverName, this.driverPic, this.driverRating,
      this.taxiDetails);

  const TaxiDriver.named(
    {
      required this.id,
      required this.driverName,
      required this.driverPic,
      required this.driverRating,
      required this.taxiDetails
    }
  );

  @override
  List<Object> get props =>
      [id, driverName, driverPic, driverRating, taxiDetails];
}
